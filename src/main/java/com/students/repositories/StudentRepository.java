package com.students.repositories;


import com.students.entities.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {


    @Query("select s from Student s where s.name like %?1%")
    Iterable<Student> findByName(String name);
}
