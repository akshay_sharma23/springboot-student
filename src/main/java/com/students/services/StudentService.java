package com.students.services;


import com.students.entities.Student;

public interface StudentService {

    Iterable<Student> listAllStudents();

    Student getStudentById(Integer id);

    Student saveStudent(Student student);

    void deleteStudent(Integer id);

    Iterable<Student> listStudentsByName(String name);


    //Student getOneDetail(Integer id);
}
