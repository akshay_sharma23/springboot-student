package com.students.services;


import com.students.entities.Student;
import com.students.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    public void setStudentRepository(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Iterable<Student> listAllStudents() {
        return studentRepository.findAll();
    }


    @Override
    public Student getStudentById(Integer id) {
        return studentRepository.findOne(id);
    }

    @Override
    public Student saveStudent(Student student) {
        return studentRepository.save( student );
    }

    @Override
    public void deleteStudent(Integer id) {
        studentRepository.delete(id);
    }


    @Override
    public Iterable<Student> listStudentsByName(String name) {
        return studentRepository.findByName ( name );
    }


}
