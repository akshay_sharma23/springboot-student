package com.students.controllers;

import com.students.entities.Student;
import com.students.repositories.StudentRepository;
import com.students.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


@Controller
public class StudentController {

    private StudentService studentService;
    @Autowired
    StudentRepository studentRepository;


    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @Value("${upload.path}")
    private String path;


    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("students", studentService.listAllStudents());
        System.out.println("Returning students:");
        return "students";
    }


    @RequestMapping("student/{id}")
    public String showProduct(@PathVariable Integer id, Model model) {
        model.addAttribute("student", studentService.getStudentById(id));
        return "studentshow";
    }

    @RequestMapping("student/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        model.addAttribute("student", studentService.getStudentById(id));
        return "studentform";
    }

    @RequestMapping("student/new")
    public String newProduct(Model model) {
        model.addAttribute("student", new Student ());
        return "studentform";
    }

    @RequestMapping(value = "student", method = RequestMethod.POST)
    public String saveProduct(Student student) {
        studentService.saveStudent ( student );
        return "redirect:/student/" + student.getId();
    }

    @RequestMapping("student/delete/{id}")
    public String delete(@PathVariable Integer id) {
        studentService.deleteStudent(id);
        return "redirect:/students";
    }

    @RequestMapping(value = "student", method = RequestMethod.GET)
    public String showStudentByname(@RequestParam (value = "name", required = false) String name, Model model) {
        model.addAttribute("search", studentService.listStudentsByName ( name ));
        return "search";
    }


    @RequestMapping(value = "/Upload", method = RequestMethod.POST)
    public String upload(@RequestParam MultipartFile file) throws IOException {

            String fileName = file.getOriginalFilename();
            InputStream is = file.getInputStream();

            Files.copy(is, Paths.get(path + fileName), StandardCopyOption.REPLACE_EXISTING);

        return "Students";
    }
}
